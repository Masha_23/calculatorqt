import sys
from classes import Op 
from object import *
from PyQt5 import QtCore, QtGui, QtWidgets
import re


# ========================================= global variables ===========================================
k = 1
operators = ['+','-','*','/','(',')']
cache = []
# ========================================== functions =================================================
def str_to_list(string):
	global operators
	l = []
	res = ''
	k = 0
	for c in string:
		if c not in operators:
			res += c
		else:
			if (c == '-' and string[k-1] in operators and string[k+1] not in operators) or (c == '-' and k == 0):
				res += c
			else:
				if res != '':
					l.append(res)
				l.append(c)
				res = ''
		k += 1
	l.append(res)
	if l[-1] == '':
		l.pop() 
	return l

def brackets(expression):
	if ')' in expression:
		y = expression.index(')')
		temp = expression[:y]
		temp.reverse()
		x = len(temp) - temp.index('(')-1
		
		return(x,y)

def oper(expression,ind,op):
	global k
	global cache
	var_1 =  expression[ind - 1]
	var_2 = expression[ind + 1]
	
	if op == '*':
		res = Op(var_1) * Op(var_2)
	
	elif op == '/':
		res = Op(var_1) / Op(var_2)
	
	elif op == '+':
		res = Op(var_1) + Op(var_2)

	elif op == '-':
		res = Op(var_1) - Op(var_2)

	cache.append(str(k) + ' ) ' + str(var_1) +' '+ str(op) +' '+str(var_2) + ' = ' + str(res))
	
	k += 1
	expression[ind] = str(res)
	expression.remove(var_1)
	expression.remove(var_2)
	return expression

def priority(expression):
	while True:
		if '*' in expression:
			i = expression.index('*')
		else:
			i = 0

		if '/' in expression:
			j = expression.index('/')
		else:
			j = 0

		if i or j:
			if i < j:
				if i != 0:
					expression = oper(expression,i,'*')
				else:
					expression = oper(expression,j,'/')

			elif j < i:
				if j != 0:
					expression = oper(expression,j,'/')
				else:
					expression = oper(expression,i,'*')

		else:
			break

	return expression

def calculate(expression):
	expression = priority(expression)
	while True:
		for c in expression:
			if c == '+':
				expression = oper(expression,expression.index(c),'+')
				break

			elif c == '-':
				expression = oper(expression,expression.index(c),'-')
				break
		if len(expression) == 1:
			break
		else:
			t = True
			for c in operators:
				if c in expression:
					t = True
					break
				else:
					t = False
			if not t:
				for  i in range(len(expression)-1):
					expression.insert(i+1,'+')

	return expression


# ================================== class ============================================   
class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent = None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.ui.push.clicked.connect(lambda:self.Main("push"))
        self.ui.clear.clicked.connect(lambda:self.Clear("clear"))

        self.ui.input.textChanged.connect(self.Check)

    def Check(self):
    	text =self.ui.input.text()
    	if not re.findall(r"([a-zA-z]+)",text):
    		if text != '':
	    		self.ui.push.setDisabled(False)
	    		self.ui.clear.setDisabled(False)
	    		self.ui.textEdit.setDisabled(False)
	    		self.ui.textEdit.clear()
	    		self.ui.output.setText('')
	    	else:
	    		self.ui.push.setDisabled(True)
	    		self.ui.clear.setDisabled(True)
	    	self.ui.error_msg.setText('')

    	else:
    		self.ui.error_msg.setText('input right expression')

    def Main(self,event):
    	global text
    	global k 
    	k = 1
    	global cache
    	cache = []

    	text = str(self.ui.input.text())
    	text_to_list = str_to_list(text)
    	
    	while '(' in text_to_list:
    		get_brackets = brackets(text_to_list)
    		expression = text_to_list[get_brackets[0]+1:get_brackets[1]]
    		expression = calculate(expression)
    		x  = get_brackets[0]
    		y = get_brackets[1]
    		temp = text_to_list[:x]
    		temp.extend(expression)
    		temp.extend(text_to_list[y+1:])
    		text_to_list = temp
    	
    	if len(text_to_list) > 1:
    		text_to_list = calculate(text_to_list)
    	self.ui.output.setText(text_to_list[0])
    	for c in cache:
    		self.ui.textEdit.insertHtml(c)
    		self.ui.textEdit.insertHtml('<br><br>')
    	self.ui.push.setDisabled(True)
    
    def Clear(self,event):
    	self.ui.input.setText('')
    	self.ui.output.setText('')
    	self.ui.textEdit.clear()
    	self.ui.output.setDisabled(True)
    	self.ui.push.setDisabled(True)
    	self.ui.clear.setDisabled(True)
	



# ============================ start ======================================================    
if __name__=="__main__":
	global app
	app = QtWidgets.QApplication(sys.argv)
	myapp = MyWin()
	myapp.show()
	sys.exit(app.exec_())
