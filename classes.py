from decimal import *
class Op:
	def __init__(self, value):
		self.value = Decimal(value)

	def __add__(self, operand):
		return self.value + operand.value

	def __sub__(self, operand):
		return self.value - operand.value

	def __mul__(self, operand):
		return self.value * operand.value

	def __truediv__(self, operand):
		return self.value / operand.value